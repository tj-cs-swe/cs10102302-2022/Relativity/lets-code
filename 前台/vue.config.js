module.exports = {
    // 部署应用时的基本 URL
    publicPath: './',
    css: {
      loaderOptions: {
        less: {
          lessOptions: {
            modifyVars: {
              'primary-color': '#517E55',
              'link-color': '#517E55',
              'btn-border-radius-sm': '0px',
            },
            javascriptEnabled: true,
          },
        },
      },
    },
    configureWebpack: {},
    chainWebpack: config => {
        config.module
            .rule("vue")
            .use("vue-loader")
            .loader("vue-loader")
            .tap(options => {
                options.compilerOptions.directives = {
                    html(node, directiveMeta) {
                        (node.props || (node.props = [])).push({
                            name: "innerHTML",
                            value: `xss(${directiveMeta.value})`
                        });
                    }
                };
                return options;
            });
    },
    devServer: {
        proxy: {
            '/api': {
                // 目标 API 地址
                target: 'http://120.92.165.181:8080',
                // 将主机标头的原点更改为目标URL
                changeOrigin: true,
                pathRewrite: {
                    '^/api': ''
                }
            }
        },
        port: '3030'
    },
    lintOnSave:false
  };
