import Vue from 'vue'
import Antd from 'ant-design-vue'
import 'ant-design-vue/dist/antd.less'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from './api'
import VueAxios from 'vue-axios'
import moment from "moment";
import myxss from "./libs/xss";

Vue.use(VueAxios, axios)
Vue.use(Antd);

Vue.$message = Vue.prototype.$message
Vue.$axios = Vue.prototype.$axios = axios
Vue.prototype.$moment = moment
Vue.prototype.xss = val => myxss.process(val)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  components: { App },
  template: '<App/>',
  router,
  store,
  render: h => h(App)
}).$mount('#app')


