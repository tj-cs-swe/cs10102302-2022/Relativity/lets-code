import user from './user'
import common from './common'
import discuss from './discuss'
import notice from './notice'

export default {user, common, discuss, notice}