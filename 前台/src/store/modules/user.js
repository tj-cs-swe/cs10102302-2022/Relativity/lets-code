import axios from "@/api";
export default {
    namespaced: true,
    state: {
        user_info: {
            user_name: '',
            user_id: '',
            phone: '',
            email: ''
        },
        timer: null
    },
    actions: {
        getUserInfo({state, dispatch}) {
            axios.get('info/user').then(res => {
                if (res.code === undefined) {
                    state.user_info = {...res}
                    if (sessionStorage['noticeTimer']) {
                        clearTimeout(sessionStorage['noticeTimer'])
                    }
                    dispatch('notice/getNoticeList', null, {root: true})
                    sessionStorage['noticeTimer'] = setTimeout(() => {
                        dispatch('notice/getNoticeList', null, {root: true})
                    }, 60000*3)
                }
            })
        }
    }
}