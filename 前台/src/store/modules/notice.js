import axios from "@/api";

export default {
    namespaced: true,
    state: {
        noticeList: []
    },

    getters: {
       noReadNoticeCount: (state) => {
           return state.noticeList.filter(item => {
               return item.mail_status == 0
           }).length
       }
    },

    actions: {
        getNoticeList({state}) {
            axios.get('site/mail/query').then((res) => {
                if (res.code === undefined) {
                    state.noticeList = res
                }
            });
        }
    }
}