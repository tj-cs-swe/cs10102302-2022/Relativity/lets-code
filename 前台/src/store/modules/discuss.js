import axios from "@/api";

export default {
    namespaced: true,
    state: {
        discussTab: '1',
        post_id: '',
        commentList: [],
        dataLoaded: false
    },

    actions: {
        async getCommentList({state}) {
            state.dataLoaded = false
            await axios.get(`comment/${state.post_id}`).then((res) => {
                if (res.code === undefined) {
                    state.commentList = res
                }
            }).catch(() => {});
            state.dataLoaded = true
        }
    }
}