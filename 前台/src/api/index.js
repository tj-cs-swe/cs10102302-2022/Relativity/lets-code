import service from '../libs/fetch'

const axios = {
  post: (url, data, options) => {
    return service.request({
      url: url,
      data: data,
      method: 'post',
      headers: {...options}
    })
  },

  get: (url, data, options) => {
    return service.request({
      url: url,
      params: data,
      method: 'get',
      headers: {...options}
    })
  }
}

export default axios
