package com.ooo.stu;

import lombok.Data;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
public class Game {
    private String oid;
    private String title;
    private Date startDate;
    private Date endDate;
    private String duration;
    private String link;
    private String group;
    private String difficulty;
    private String platform;
}
