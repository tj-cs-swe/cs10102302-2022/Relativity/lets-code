package com.ooo.stu;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@SpringBootApplication
@EnableScheduling
public class StuApplication implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(StuApplication.class, args);
	}


	@Autowired
	JdbcTemplate jdbcTemplate;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		runTask();
	}

	@Scheduled(cron = "5 0/1 * * * ?")
	public void runTask(){
		List<Game> webs = new ArrayList<>();
		webs.addAll(NowCoder.findGame());
		webs.addAll(CodeForces.findGame());
		webs.addAll(Letcode.findGame());
		webs.addAll(Luogu.findGame());
		webs.addAll(AtCoder.findGame());

		System.out.println("webs: ==== ");
		for(Game g: webs) {
			System.out.println(g);
		}

		List<Game> dbGames = getDB();
		for(Game g: webs) {
			boolean isExist = isExist(g.getOid(),dbGames);
			if (isExist) {
				if(!isEqual(g,getByOid(g.getOid(),dbGames))) {
					update(g);
				}
			} else {
				insert(g);
			}
			System.out.println(g);
		}
	}

	private boolean isExist(String oid, List<Game> dbGames){
		for (Game g: dbGames) {
			if(g.getOid() != null && oid.equals(g.getOid()))
				return true;
		}
		return false;
	}

	private Game getByOid(String oid, List<Game> dbGames){
		for (Game g: dbGames) {
			if(oid.equals(g.getOid()))
				return g;
		}
		return null;
	}
	private boolean isEqual(Game web,  Game  dbGame){
		if(!StringUtils.equals( web.getTitle() , dbGame.getTitle())
		 || !StringUtils.equals( web.getLink() , dbGame.getLink())
				|| web.getStartDate().getTime() != dbGame.getStartDate().getTime()
				|| web.getEndDate().getTime() != dbGame.getEndDate().getTime()
		 || !StringUtils.equals( web.getDuration() , dbGame.getDuration())

		) {
			return false;
		}
		return true;
	}

	private Game insert(Game game){
		jdbcTemplate.update("insert into game (game_name,game_start_time,game_end_time," +
						"duration,checked," +
						"website,game_type,level_,platform,cust_user_id,cust_public,oid)" +
						" values (?,?,?,?,?, ?,?,?,?,?, ?,?)",
				game.getTitle(), game.getStartDate(),game.getEndDate(),game.getDuration(), 1,
				game.getLink(),game.getGroup(),game.getDifficulty(),game.getPlatform(),0,1,
				game.getOid());
		return null;
	}

	private Game update(Game g) {
		jdbcTemplate.update("update game set game_name = ?, game_start_time=?, game_end_time=?, " +
						"duration = ?, website=?, game_type=?,platform=?  " +
						"where oid = ?",
				g.getTitle(), g.getStartDate(), g.getEndDate(),
				g.getDuration(),g.getLink(),g.getGroup(),g.getPlatform(),
				g.getOid());
		return null;
	}

	private List<Game> getDB(){
		List<Game> dbGames = jdbcTemplate.query("select * from game where  oid is not null", new RowMapper<Game>() {
			@Override
			public Game mapRow(ResultSet rs, int rowNum) throws SQLException {
				Game g = new Game();
				g.setTitle(rs.getString("game_name"));
				g.setGroup(rs.getString("game_type"));
				g.setStartDate(rs.getTimestamp("game_start_time"));
				g.setEndDate(rs.getTimestamp("game_end_time"));
				g.setDuration(rs.getString("duration"));
				g.setLink(rs.getString("website"));
				g.setOid(rs.getString("oid"));
				return g;
			}
		});
		return dbGames;
	}
}
