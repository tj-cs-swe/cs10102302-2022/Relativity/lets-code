package com.ooo.stu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Letcode {

    public static List<Game> findGame() {
        List<Game> games = new ArrayList<>();
        try {
            String data = " {\"operationName\":null,\"variables\":{},\"query\":\"{\\n  contestUpcomingContests {\\n    containsPremium\\n    title\\n    cardImg\\n    titleSlug\\n       startTime\\n    duration\\n    originStartTime\\n    isVirtual\\n    isLightCardFontColor\\n    company {\\n      watermark\\n      __typename\\n    }\\n    __typename\\n  }\\n}\\n\"} ";
            Connection.Response document = Jsoup.connect("https://leetcode.cn/graphql")
                    .header("Accept","*/*")
                    .header("content-type","application/json")
                    .ignoreContentType(true)
                    .requestBody(data)
                    .method(Connection.Method.POST)
                    .execute();
            String body = document.body();

            body = StringEscapeUtils.unescapeJava(body);



            System.out.println(body);

            JSONArray datas = JSON.parseObject(body).getJSONObject("data").getJSONArray("contestUpcomingContests");
            for(int i=0;i<datas.size();i++) {
                JSONObject obj = datas.getJSONObject(i);
                String name = obj.getString("title");
                Long startTimeS = obj.getLong("startTime");
                Long durationS = obj.getLong("duration");
                Date startTime = new Date(startTimeS * 1000);
                Date endTime = new Date(startTime.getTime() + durationS * 1000);

                String duration = TimeUtils.getDatePoor(startTime,endTime);

                String titleSlug = obj.getString("titleSlug");
                String href = "https://leetcode.cn/contest/" + titleSlug;
                String group = "";
                String difficulty = "1";
                String oid = "leetcode-" + titleSlug;
                Game game = new Game();
                game.setTitle(name);
                game.setStartDate(startTime);
                game.setEndDate(endTime);
                game.setDuration(duration);
                game.setLink(href);
                game.setGroup(group);
                game.setOid(oid);
                game.setDifficulty(difficulty);
                game.setPlatform("力扣");
                games.add(game);
            }
            for(Game g: games) {
                System.out.println(g);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return games;
    }

}
