from flask import Blueprint, request, session, jsonify, current_app
from sqlalchemy.exc import IntegrityError
from module.user import User
from module.comment import Comment
from module.comment_statistic import CommentStatistic
from controller.site_mail import site_mail_add
from controller.user import user_disabled

comment=Blueprint('comment',__name__)

@comment.route('/comment/add', methods=['POST'])
def comment_add():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    post_id=request.json.get('post_id')
    content=request.json.get('content')
    if not content:
        return {"info": "评论为空", "code": 501}
    if len(content) > 4000:
        return {"info": "评论超长超长", "code": 501}
    
    content = content.strip()
    
    try:
        Comment.insert_comment(u_id, post_id, content)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "系统出错啦", "code": 501}
    return {"info": "success" ,"code": 0}


@comment.route("/comment/delete", methods=['POST'])
def comment_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    comment_id = request.json.get("comment_id")
    if not comment_id:
        return {"info": "comment_id is null" ,"code": 500}
    
    comment = Comment.get_by_id(comment_id)
    if not comment:
        return {"info": "评论不存在" ,"code": 501}
    
    comments = Comment.get_by_reply_id(comment_id)
    if not comments:
        comments = Comment.get_by_sub_reply_id(comment_id)
    while comments:
        tmp_comments = []
        for comment in comments:
            tmp = Comment.get_by_sub_reply_id(comment.comment_id)
            tmp_comments += tmp
            CommentStatistic.delete_by_comment_id(comment.comment_id)
            Comment.delete_by_comment_id(comment.comment_id)
        comments = tmp_comments
    
    CommentStatistic.delete_by_comment_id(comment_id)
    Comment.delete_by_comment_id(comment_id)
    
    user = User().get_by_id(u_id)
    current_app.logger.info('current user [%s]' % user)
    if comment.user_id != user.user_id and user.user_type == 'admin':
        current_app.logger.info('user_type [%s]' % user.user_type)
        mail_content = '您好，您的评论<<%s...>>被管理员删除' % (comment.content[0:100])
        site_mail_add(comment.user_id, '您的评论被管理员删除', mail_content)
    return {"info": "success" ,"code": 0}


@comment.route('/comment/reply',methods=['POST'])
def reply():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    post_id = request.json.get('post_id')
    comment_id = request.json.get('comment_id')
    content = request.json.get('content')
    reply_id = request.json.get('reply_id')
    if reply_id is None:
        reply_id = 0
    ipaddr = request.remote_addr

    #评论字数限制
    if not content or len(content) > 200:
        return {"info": "评论超长" ,"code": 501}

    comment=Comment()
    try:
        comment.insert_reply(u_id, post_id, comment_id, reply_id, content)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "系统出错啦" , "code": 501}
    return {"info": "success" ,"code": 0} #回复成功


#为了使用ajax分页
#由于分页栏已经完成渲染，此接口仅根据前端的页码请求后台对应数据
@comment.route('/comment/<int:post_id>')
def comment_page(post_id):
    page_num = request.args.get("page_num")
    if page_num is None:
        page_num = 1
    else:
        page_num = int(page_num)
    page_size = request.args.get("page_size")
    if page_size is None:
        page_size = 10
    else:
        page_size = int(page_size)
    
    resp = []
    comments = Comment.get_comment_list_with_user(post_id, page_num, page_size)
    current_app.logger.info('lens[%s]' % len(comments.items))
    for comment, user in comments.items:
        data = {}
        data['comment_id'] = comment.comment_id
        data['user_id'] = user.user_id
        data['post_id'] = comment.post_id
        data['user_name'] = user.user_name
        data['content'] = comment.content
        data['reply_id'] = comment.reply_id
        data['sub_reply_id'] = comment.sub_reply_id
        data['agree_count'] = comment.agree_count
        data['oppo_count'] = comment.oppo_count
        data['hidden'] = comment.hidden
        data['create_time'] = comment.create_time.strftime('%Y-%m-%d %H:%M:%S')
        replies = []
        lists = Comment.get_reply_list_with_user(comment.comment_id)
        for reply, reply_user in lists:
            current_app.logger.info('reply.comment_id[%s]' % reply.comment_id)
            sub_data = reply.to_json()
            sub_data.update({'user_name': reply_user.user_name})
            replies.append(sub_data)
        data['replies'] = replies
        resp.append(data)
    return jsonify(resp)


@comment.route('/comment/agree', methods=['POST'])
def comment_agree():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    post_id = request.json.get('post_id')
    if post_id is None:
        post_id = 0
    
    comment_id = request.json.get('comment_id')
    
    statistic = CommentStatistic()
    statistic.user_id = u_id
    statistic.post_id = post_id
    statistic.comment_id = comment_id
    statistic.statistic_type = 0
    try:
        statistic.add()
    except IntegrityError as e:
        return {"info": "您已经点过赞了" , "code": 501}
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error" , "code": 500}
    
    try:
        Comment.comment_agree(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error" , "code": 500} #新增失败
    return {"info": "success" ,"code": 0}


@comment.route('/comment/oppo', methods=['POST'])
def comment_oppo():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    post_id = request.json.get('post_id')
    if post_id is None:
        post_id = 0
    
    comment_id = request.json.get('comment_id')
    
    statistic = CommentStatistic()
    statistic.post_id = post_id
    statistic.user_id = u_id
    statistic.comment_id = comment_id
    statistic.statistic_type = 1
    try:
        statistic.add()
    except IntegrityError as e:
        return {"info": "您已经反对过了" , "code": 501}
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error" , "code": 500}
    
    try:
        Comment.comment_oppo(comment_id)
    except Exception as e:
        current_app.logger.error(e)
        return {"info": "error" , "code": 500} #新增失败
    return {"info": "success" ,"code": 0}