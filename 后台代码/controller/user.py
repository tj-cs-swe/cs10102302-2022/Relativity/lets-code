import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify, current_app
from werkzeug.security import generate_password_hash, check_password_hash
from common.utility import *
from common.sms import *
from module.game import Game
from module.user import *
from common import user_check
import random
import uuid
import re
from cachelib import SimpleCache

phonenum_re = re.compile(r'^(13[0-9]|14[0|5|6|7|9]|15[0|1|2|3|5|6|7|8|9]|'
                        r'^16[2|5|6|7]|17[0|1|2|3|5|6|7|8]|18[0-9]|'
                        r'19[1|3|5|6|7|8|9])\d{8}$')

mail_re = re.compile(r'^[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+){0,4}@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]{2,12}){0,4}$')


str_re = re.compile(r'^[a-zA-Z0-9_-~`!@#\$%\^&*\(\)+=,\.?,<>]+$')


user=Blueprint('user',__name__)

cache_timeout = 120
register_code_cache = SimpleCache()
check_code_cache = SimpleCache()


@user.route('/register',methods=['GET','POST'])
def register():
    if request.method == 'POST':
        name = request.json.get('name')
        if not name:
            return {"info": "username is null", "code": 500}
        if len(name) > 50:
            return {"info": "用户名超长", "code": 501}
        if len(name) < 5:
            return {"info": "用户名太短", "code": 501}
        
        if not str_re.search(name):
            return {"info": "用户名非法", "code": 501}
        
        if not user_check.check_user_name(name):
            return {"info": "用户名被禁止", "code": 501}
        
        if User().get_by_username(name):
            return {"info": "用户名被占用", "code": 501}
        
        password = request.json.get('password')
        if not password:
            return {"info": "password is null", "code": 500}
        if len(password) > 32:
            return {"info": "密码超长", "code": 501}
        if len(password) < 5:
            return {"info": "密码太短", "code": 501}
        
        if not str_re.search(password):
            return {"info": "密码包含非法字符", "code": 501}
        
        register_code = request.json.get('register_code')
        if not register_code:
            return {"info": "register_code is null", "code": 500}
        
        email = request.json.get('email')
        if email:
            if not mail_re.search(email):
                return {"info": "邮箱非法", "code": 501}
            
            if User().get_by_mail(email):
                return {"info": "邮箱被占用", "code": 501}
            rc = register_code_cache.get(email)
            if not rc:
                return {"info": "验证码已经过期", "code": 501}
            register_code_cache.delete(email)
        phonenum = request.json.get('phonenum')
        if phonenum:
            if not phonenum_re.search(phonenum):
                return {"info": "手机号非法", "code": 501}
            
            if User().get_by_phone(phonenum):
                return {"info": "手机号被占用", "code": 501}
            rc = register_code_cache.get(phonenum)
            if not rc:
                return {"info": "验证码已经过期", "code": 501}
            register_code_cache.delete(phonenum)
        
        if not email and not phonenum:
            return {"info": "email and  phonenum is null", "code": 500}
        
        if register_code != rc:
            return {"info": "register_code invalid, please input again", "code": 500}
        
        pwhash = generate_password_hash(password,method='pbkdf2:sha1', salt_length=8)
        user = User(user_name=name,password=pwhash, mail=email, phone=phonenum,user_type='reg')
        user.add()
        return {"info": "success", "code": 0}
    return {"info": "waiting for request", "code": 0}

@user.route('/login',methods=['GET','POST'])
def login():
    if request.method == 'POST':
        name = request.json['name']
        password = request.json['password']

        user = User().get_by_username(name)
        if not user:
            return {"info": "no such user", "code": 1}

        print(check_password_hash(user.password,password))

        if not check_password_hash(user.password,password):
            return {"info": "wrong password", "code": 2}
        
        if user.black == 1:
            return {"info": "user is disabled", "code": 3}
        
        session['isLogin'] = 'true'
        session['user_id'] = user.user_id
        session['user_name'] = name
        session['type'] = user.user_type
        token = user.user_id

        return {"info": "success", "code": 0, 'result': {'token': token}}

@user.route("/logout")
def logout():
    session['isLogin'] = 'false'
    session['user_id'] = 0
    session['user_name'] = ''
    session['type'] = ''
    return {"info": "success" ,"code": 0}


# 修改个人信息
@user.route('/alter',methods=['POST'])
def modify():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    user_name=request.json.get('user_name_new')
    if user_name:
        if len(user_name) > 50:
            return {"info": "用户名超长", "code": 501}
        if len(user_name) < 5:
            return {"info": "用户名太短", "code": 501}
        
        if not str_re.search(user_name):
            return {"info": "用户名非法", "code": 501}
        
        if not user_check.check_user_name(user_name):
            return {"info": "用户名被禁止", "code": 501}
    
    mail=request.json.get('mail')
    if mail:
        if not mail_re.search(mail):
            return {"info": "邮箱非法", "code": 501}
        
        if User().get_by_mail(mail):
            return {"info": "邮箱被占用", "code": 501}
    
    preferrence=request.json.get('preferrence')
    if preferrence:
        preferrence = preferrence.strip()
        if len(preferrence) > 50:
            return {"info": "爱好超长", "code": 501}
    
    phone = request.json.get('phone')
    if phone:
        if not phonenum_re.search(phone):
            return {"info": "手机号非法", "code": 501}
        
        if User().get_by_phone(phone):
            return {"info": "手机号被占用", "code": 501}

    return User().modify_info(u_id,user_name,mail,preferrence,phone) #1为修改失败 ，0为修改成功


@user.route('/info/user',methods=['GET'])
def show_info():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    user_name = session.get('user_name')
    if not user_name:
        return {"info": "not login" ,"code": 1} #未等录
    
    user=User().get_by_username(user_name)
    dict={}
    for k,v in user.__dict__.items():
        if not k.startswith('_sa'):
            dict[k]=v

    return jsonify(dict)

@user.route('/sendset')
def set_send():
    send_time=request.form.get('send_time').strip()
    send_way=request.form.get('send_way').strip()

    user_id=request.json['user_id']
    try:
        dbsession.query(User).filter_by(user_id=user_id).update({'send_time':send_time})
        dbsession.query(User).filter_by(user_id=user_id).update({'send_way':send_way})
        dbsession.commit()
        dbsession.close()
        return {"info": "success" ,"code": 0}
    except:
        return {"info": "error" ,"code": 1} #修改失败

#以下均为管理员操作

#禁用用户权限/恢复用户权限
@user.route('/black/<user_id>')
def set_black_white(user_id):
    u_id=request.form.get('user_id')
    black=request.form.get('black')

    if u_id==None or black==None:
        return {"info":"error","code":2}  #前端获取信息不完整

    try:
        dbsession.query(User).filter_by(user_id=user_id).update({"black":black})
        dbsession.commit()
        dbsession.close()
        return {"info": "success", "code": 0}

    except:
        return {"info": "error", "code": 1}  # 修改失败


@user.route('/info/unchecked-game')
def show_unchecked_game():
    res=Game().get_all_uchecked()
    for i in res:
        print(i.__dict__)

    list=[]

    for i in res:
        dict={}
        for k,v in i.__dict__.items():
            if not k.startswith('_sa_instance_state'):
                dict[k]=v
        list.append(dict)

    return  jsonify(list)

@user.route('/check/<game_id>')
def check_game(game_id):
    try:
        Game().set_checked_by_id(game_id)
        return {"info":"success","code":0}
    except:
        return {"info": "error", "code": 1}  # 修改失败


@user.route('/register/email/register_code', methods=['POST'])
def email_send_register_code():
    email = request.json.get('email')
    if not email:
        return {"info": "email is null", "code": 500}
    
    if not mail_re.search(email):
        return {"info": "邮箱非法", "code": 501}
    
    register_code = gen_register_code()
    try:
        send_email(email, register_code)
    except Exception as e:
        current_app.logger.error("send_email failed %s" % e)
        return {"info": "send mail failed", "code": 500}
    
    register_code_cache.set(email, register_code, cache_timeout)
    return {"info" : "success", "code" : 0}


@user.route('/register/phone/register_code', methods=['POST'])
def phone_send_register_code():
    phonenum = request.json.get('phonenum')
    if not phonenum:
        return {"info": "phonenum is null", "code": 500}
    
    if not phonenum_re.search(phonenum):
        return {"info": "手机号非法", "code": 501}
    
    register_code = gen_register_code()
    try:
        sms_send_code(phonenum, register_code)
    except Exception as e:
        current_app.logger.error("sms_send_code failed %s" % e)
        return {"info": "send message failed", "code": 500}
    
    register_code_cache.set(phonenum, register_code, cache_timeout)
    return {"info" : "success", "code" : 0}


@user.route('/register/email/check', methods=['POST'])
def email_check():
    email = request.json.get('email')
    if not email:
        return {"info": "phonenum is null", "code": 500}
    
    if not mail_re.search(email):
        return {"info": "邮箱非法", "code": 501}
    
    if User().get_by_mail(email):
        return {"info": "邮箱被占用", "code": 501}
    return {"info" : "success", "code" : 0}


@user.route('/register/phone/check', methods=['POST'])
def phonenum_check():
    phonenum = request.json.get('phonenum')
    if not phonenum:
        return {"info": "phonenum is null", "code": 500}
    
    if not phonenum_re.search(phonenum):
        return {"info": "手机号非法", "code": 501}
    
    if User().get_by_phone(phonenum):
        return {"info": "手机号被占用", "code": 501}
    return {"info" : "success", "code" : 0}


@user.route('/register/user/check', methods=['POST'])
def name_check():
    name = request.json.get('name')
    if not name:
        return {"info": "name is null", "code": 500}
    if len(name) > 50:
        return {"info": "用户名超长", "code": 501}
    if len(name) < 5:
        return {"info": "用户名太短", "code": 501}
    
    if not str_re.search(name):
        return {"info": "用户名非法", "code": 501}
    
    if not user_check.check_user_name(name):
        return {"info": "用户名被禁止", "code": 501}
    
    if User().get_by_username(name):
        return {"info": "用户名被占用", "code": 501}
    return {"info" : "success", "code" : 0}
        

@user.route('/register/forget_password', methods=['POST'])
def forget_password_email():
    name = request.json.get('name')
    if name:
        name = name.strip()
    else:
        return {"info": "name is null", "code": 500}
    
    user = User().get_by_username(name)
    if not user:
        return {"info": "用户不存在", "code": 501}
    
    if not user.mail:
        return {"info": "未留邮箱", "code": 501}
    
    check_code = str(uuid.uuid4()).replace('-', '')
    check_code_cache.set(name, check_code, cache_timeout)
    
    url = 'http://47.101.63.85/cloudItem/index.html#/register/reset_password?check_code=%s&name=%s' % (check_code, name)
    
    try:
        send_email_reset_password(user.mail, url)
    except Exception as e:
        current_app.logger.error("send_email failed %s" % e)
        return {"info": "send mail failed", "code": 500}
    
    return {"info" : "success", "code" : 0}


@user.route('/register/forget_password/sms', methods=['POST'])
def forget_password_sms():
    name = request.json.get('name')
    if name:
        name = name.strip()
    else:
        return {"info": "name is null", "code": 500}
    
    user = User().get_by_username(name)
    if not user:
        return {"info": "用户不存在", "code": 501}
    
    if not user.phone:
        return {"info": "未留手机号", "code": 501}
    
    check_code = gen_register_code()
    check_code_cache.set(name, check_code, cache_timeout)
    
    try:
        sms_send_code(user.phone, check_code)
    except Exception as e:
        current_app.logger.error("sms_send_code failed %s" % e)
        return {"info": "send message failed", "code": 500}
    
    return {"info" : "success", "code" : 0}


@user.route('/register/reset_password', methods=['POST'])
def reset_password():
    check_code = request.json.get('check_code')
    if check_code:
        name = request.json.get('name')
        if name:
            name = name.strip()
        else:
            return {"info": "name is null", "code": 500}
        
        cc = check_code_cache.get(name)
        if not cc:
            return {"info": "check_code expired, please reset again", "code": 500}
        if check_code != cc:
            return {"info": "check_code invalid, please input again", "code": 500}
        check_code_cache.delete(name)
    
    else:
        name = session.get('user_name')
        if not name:
            return {"info": "not login" ,"code": 1} #未等录
    
    password = request.json.get('password')
    if password:
        password = password.strip()
    else:
        return {"info": "password is null", "code": 500}

    pwhash = generate_password_hash(password,method='pbkdf2:sha1', salt_length=8)
    user = User.modify_password(user_name=name,password=pwhash)
    return {"info" : "success", "code" : 0}


@user.route('/user/query', methods=['GET'])
def user_query():
    name = session.get('user_name')
    if not name:
        return {"info": "not login" ,"code": 1} #未等录
    
    resp = []
    users = User.query()
    for user in users:
        resp.append(user.to_json())
    return jsonify(resp)


@user.route('/user/disable', methods=['POST'])
def user_disable():
    name = session.get('user_name')
    if not name:
        return {"info": "not login" ,"code": 1} #未等录
    
    user_id = request.json.get('user_id')
    if user_id is None:
        return {"info": "user_id is null", "code": 500}
    
    User.modify_black(user_id, 1)
    
    user = User().get_by_id(user_id)
    if user.mail:
        send_user_disable(user.mail, user.user_name)
    
    return {"info" : "success", "code" : 0}

@user.route('/user/open', methods=['POST'])
def user_open():
    name = session.get('user_name')
    if not name:
        return {"info": "not login" ,"code": 1} #未等录
    
    user_id = request.json.get('user_id')
    if user_id is None:
        return {"info": "user_id is null", "code": 500}
    
    User.modify_black(user_id, 0)
    
    user = User().get_by_id(user_id)
    if user.mail:
        send_user_open(user.mail, user.user_name)

    return {"info" : "success", "code" : 0}


def user_disabled(user_id):
    user = User().get_by_id(user_id)
    if user and user.black == 1:
        session['isLogin'] = 'false'
        session['user_id'] = 0
        session['user_name'] = ''
        session['type'] = ''
        return True
    else:
        return False
