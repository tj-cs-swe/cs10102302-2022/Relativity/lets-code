import hashlib
import re
from flask import Blueprint, make_response, session, request, redirect, url_for, jsonify, current_app
from module.user import *
from module.reserve import *
from module.game import *
from controller.user import user_disabled


reserve = Blueprint('reserve', __name__)


@reserve.route("/reserve/username", methods=['GET'])
def get_reserve_by_user2():
    u_id = session.get('user_id')
    current_app.logger.info('user_id:%s' % u_id)
    if not u_id:
        return {"info": "user not login", "code": 1}
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    reserves = Reserve().get_by_user(u_id)
    resp = []
    for reserve in reserves:
        resp.append(reserve.to_json())
    return jsonify(resp)


@reserve.route("/reserve/user&game", methods=['GET'])
def get_reserve_by_user():
    username = session.get('user_name')
    current_app.logger.info('username:%s' % username)
    if not username:
        return {"info": "user not login", "code": 1}
    
    user = User().get_by_username(username)
    if not user:
        return {"info": "user not exist", "code": 1}
    u_id = user.user_id
    current_app.logger.info('u_id:%s' % u_id)

    g_name = request.args.get('game_name')
    # g_id = get_game_by_name(g_name)
    game = Game().get_by_name(g_name)
    g_id = game.game_id
    current_app.logger.info('g_id:%d' % g_id)

    # reserves = find_by_user_and_game(u_id,g_id)
    reserves = Reserve().get_by_user_and_game(u_id, g_id)
    resp = []
    for reserve in reserves:
        resp.append(reserve.to_json())
    return jsonify(resp)


# 这里好像有点问题，不过也可能我错了，大家可以一起讨论下
@reserve.route("/reserve/update", methods=['POST'])
def reserve_update():
    m_reserve = Reserve(user_id=request.form.get("username").strip(), game_id=request.form.get("game_name").strip())

    # username = request.form.get("username").strip()
    # m_reserve.user_id = dbsession.query(User).filter_by(user_name=username)
    #
    # g_name = request.form.get("game_name").strip()
    # m_reserve.game_id = dbsession.query(Game).filter_by(game_name=g_name)

    m_reserve.update_reserve()
    return


@reserve.route("/reserve/delete", methods=['POST'])
def reserve_delete():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    game_id = request.json.get("game_id")
    if game_id:
        game_id = int(game_id)
        Reserve.delete_by_user_id_game_id(u_id, game_id)
        return  {"info": "success" ,"code": 0}
    
    reserve_id = request.json.get("reserve_id")
    if reserve_id:
        reserve_id = int(reserve_id)
        Reserve.delete_by_reserve_id(reserve_id)
        return  {"info": "success" ,"code": 0}
    
    return  {"info": "success" ,"code": 0}

@reserve.route("/reserve/game", methods=['GET'])
def get_reserve_game():
    u_id = session.get('user_id')
    current_app.logger.info('user_id:%s' % u_id)
    if not u_id:
        return {"info": "user not login", "code": 1}
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    reserves = Reserve().get_by_user(u_id)
    resp = []
    for reserve in reserves:
        game = Game.get_by_id(reserve.game_id)
        if game.cust_public == 0:
            if u_id != game.cust_user_id:
                continue
        resp.append(game.to_json())
    return jsonify(resp)


@reserve.route("/reserve/add", methods=['POST'])
def reserve_add():
    u_id = session.get('user_id')
    if not u_id:
        return {"info": "not login" ,"code": 1} #未等录
    
    if user_disabled(u_id):
        return {"info": "用户被封禁", "code": 502}
    
    game_id = request.json.get("game_id")
    if game_id:
        game_id = int(game_id)
    
    reserve = Reserve().get_by_user_and_game(u_id, game_id)
    if reserve:
        return  {"info": "success" ,"code": 0}
    else:
        reserve = Reserve()
        reserve.user_id = u_id
        reserve.game_id = game_id
        reserve.update_reserve()
    return  {"info": "success" ,"code": 0}
