import traceback
from flask_apscheduler import APScheduler
from flask import Blueprint, request, session, jsonify, current_app
from datetime import datetime
from module.user import User
from module.game import Game
from module.push import Push
from module.reserve import Reserve
from module.subscribe import Subscribe
from module.recommend import Recommend
from module.push_content import PushContent
from common.utility import *
from controller.site_mail import site_mail_add


remaining_times = {
    '1' : '3天',
    '2' : '2天',
    '3' : '1天',
    '4' : '1小时',
    '5' : '半小时',
}


def Singleton(cls):
    _instance = {}
    
    def _singleton(*arg, **args):
        if cls not in _instance:
            _instance[cls] = cls(*arg, **args)
        return _instance[cls]
        
    return _singleton


@Singleton
class Scheduler(APScheduler):
    pass


scheduler = Scheduler()

interval = 60

@scheduler.task('interval', id = 'job-1', seconds = interval)
def push_game():
    with scheduler.app.app_context():
        try:
            current_app.logger.info('push game..')
            
            now = datetime.now()
            
            users = User.query()
            games = Game().get_all()
            for game in games:
                timedelta = game.game_start_time - now
                seconds = timedelta.total_seconds()
                #current_app.logger.info('game_id[%s], seconds[%s]' % (game.game_id, seconds))
                if seconds >= 3 * 24 * 3600 - interval and seconds <= 3 * 24 * 3600 + interval:
                    push_time = '1'
                elif seconds >= 2 * 24 * 3600 - interval and seconds <= 2 * 24 * 3600 + interval:
                    push_time = '2'
                elif seconds >= 24 * 3600 - interval and seconds <= 24 * 3600 + interval:
                    push_time = '3'
                elif seconds >= 3600 - interval and seconds <= 3600 + interval:
                    push_time = '4'
                elif seconds >= 1800 - interval and seconds <= 1800 + interval:
                    push_time = '5'
                else:
                    continue
                
                current_app.logger.info('game_id[%s], seconds[%s]' % (game.game_id, seconds))
                
                remaining_time = remaining_times.get(push_time)
                
                for user in users:
                    if user.black == 1:
                        current_app.logger.info('user[%s] blacked' % user.user_name)
                        continue
                    
                    push = Push.get_by_user_id(user.user_id)
                    if not push:
                        current_app.logger.info('user[%s] no push' % user.user_name)
                        continue
                    if not push.push_method:
                        current_app.logger.info('user[%s] push_method is null' % user.user_name)
                        continue
                    
                    push_flag = False
                    
                    if Reserve().get_by_user_and_game(user.user_id, game.game_id):
                        push_flag = True
                    
                    if not push_flag:
                        recommend = Recommend.get_by_user_id(user.user_id)
                        if recommend:
                            if recommend.game_level and str(game.level_) in recommend.game_level:
                                push_flag = True
                            if recommend.game_platform and game.platform and game.platform in recommend.game_platform:
                                push_flag = True
                            
                            game_duration_calculated = '0'
                            if game.game_start_time and game.game_end_time:
                                time_delta = game.game_end_time - game.game_start_time
                                time_delta = time_delta.total_seconds()
                                game_duration_calculated = get_duration(time_delta)
                            if recommend.game_duration and game_duration_calculated in recommend.game_duration:
                                push_flag = True
                    
                    
                    if not push_flag:
                        subscribe = Subscribe().get_by_user_id(user.user_id)
                        if subscribe:
                            if subscribe.game_level and str(game.level_) in subscribe.game_level and subscribe.game_platform and game.platform and game.platform in subscribe.game_platform:
                                push_flag = True
                    
                    if not push_flag:
                        current_app.logger.info('user[%s], game_id[%s] not push' % (user.user_name, game.game_id))
                        continue
                    
                    current_app.logger.info('push_flag %s' % push_flag)
                    
                    push_methods = push.push_method.split(',')
                    for push_method in push_methods:
                        push_method = push_method.strip()
                        if PushContent.query_by_key(user.user_id, game.game_id, push_time, push_method):
                            current_app.logger.info('user_id[%s], game_id[%s], push_time[%s], push_method[%s] pushed' % (user.user_id, game.game_id, push_time, push_method))
                            continue
                        
                        pushContent = PushContent()
                        pushContent.user_id = user.user_id
                        pushContent.game_id = game.game_id
                        pushContent.push_time = push_time
                        pushContent.push_method = push_method
                        pushContent.add()
                        
                        if push_method == 'mail' and user.mail:
                            try:
                                send_email_game(user.mail, game.game_name, game.game_start_time.strftime('%Y-%m-%d %H:%M:%S'), remaining_time)
                            except Exception as e:
                                current_app.logger.error('%s' % traceback.format_exc())
                        if push_method == 'site':
                            site_mail_add(user.user_id, '您预约/订阅/感兴趣的赛事即将开始', '您预约/订阅/感兴趣的赛事《%s》还有%s开始，请及时查看，谢谢!' % (game.game_name, remaining_time))
        
        except Exception as e:
            current_app.logger.error('%s' % traceback.format_exc())
