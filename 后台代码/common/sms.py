import json
from tencentcloud.common import credential
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
from tencentcloud.sms.v20210111 import sms_client, models
from flask import current_app


def sms_send_code(phone_number, check_code):
    try:
        cred = credential.Credential("AKID0qQjqhHbeQwCArISKvGK8fvud5FAn9E3", "Zrsc5HCqDJ8gyzkERVIoCUXKr9AuOcZF")
        httpProfile = HttpProfile()
        httpProfile.endpoint = "sms.tencentcloudapi.com"
    
        clientProfile = ClientProfile()
        clientProfile.httpProfile = httpProfile
        client = sms_client.SmsClient(cred, "ap-guangzhou", clientProfile)
    
        req = models.SendSmsRequest()
        params = {
            "PhoneNumberSet": [ phone_number ],
            "SmsSdkAppId": "1400688533",
            "SignName": "Letscode",
            "TemplateId": "1427442",
            "TemplateParamSet": [ check_code ]
        }
        req.from_json_string(json.dumps(params))
    
        resp = client.SendSms(req)
        current_app.logger.info(resp.to_json_string())
    
    except TencentCloudSDKException as err:
        current_app.logger.info(err)
        raise err
