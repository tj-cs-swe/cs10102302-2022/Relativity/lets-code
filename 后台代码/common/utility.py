import random, string
from io import BytesIO

from PIL import Image, ImageFont, ImageDraw

mail_sender = '827887998@qq.com'
mail_password = 'dyuqjzyoqstobahf'


class ImageCode:
    def gen_text(self):
        # sample用于从一个大的列表或字符串随机取得n个字符
        list = random.sample(string.ascii_letters + string.digits, 4)
        return ''.join(list)

    def rand_color(self):
        red = random.randint(32, 127)
        green = random.randint(32, 127)
        blue = random.randint(32, 127)
        return red, green, blue

    def draw_verify_code(self):
        code = self.gen_text()
        width, height = 120, 50

        im = Image.new('RGB', (width, height), 'white')

        font = ImageFont.truetype(font='arial.ttf', size=40)
        draw = ImageDraw.Draw(im)

        for i in range(4):
            draw.text((5 + random.randint(-3, 3) + 23 * i, 5 + random.randint(-3, 3)),
                      text=code[i], fill=self.rand_color(), font=font)

        # 绘制干扰线
        self.draw_lines(draw, 3, width, height)
        # im.show()
        return im, code

    def draw_lines(self, draw, n, width, height):
        for num in range(n):
            x1 = random.randint(0, width / 2)
            x2 = random.randint(0, width)
            y1 = random.randint(0, height / 2)
            y2 = random.randint(0, height)
            draw.line(((x1, y1), (x2, y2)), fill='black', width=2)

    # 生成图片验证码并返回给控制器
    def get_code(self):
        image, code = self.draw_verify_code()
        buf=BytesIO()
        image.save(buf,format='jpeg')
        bstr=buf.getvalue() #二进制码
        return code,bstr

#邮件相关操作
from smtplib import SMTP_SSL
from email.mime.text import MIMEText
from email.header import Header

def send_email(receiver,ecode):
    content=f"<br/>欢迎注册LetsCode，您的验证码为：<span style='color:red;font-size:20px;'>{ecode}</span>，2分钟内有效，"\
    "请尽快复制此验证码完成注册，感谢您的使用!"
    #实例化邮件对象，并制定关键参数
    message=MIMEText(content,'html','utf-8')
    #指定邮件的标题
    message['Subject']=Header('LetsCode-注册验证码','utf-8')
    message['From']=mail_sender
    message['To']=receiver

    smtpOBJ=SMTP_SSL('smtp.qq.com')
    smtpOBJ.login(user=mail_sender,password=mail_password)
    smtpOBJ.sendmail(mail_sender,receiver,str(message))
    smtpOBJ.quit()

def send_email_reset_password(receiver, url):
    content=f"<br/>您好，您重置密码的链接：<span style='color:red;font-size:20px;'>{url}</span>，2分钟内有效，"\
    "请尽快点击完成重置密码!"
    #实例化邮件对象，并制定关键参数
    message=MIMEText(content,'html','utf-8')
    #指定邮件的标题
    message['Subject']=Header('LetsCode-重置密码','utf-8')
    message['From']=mail_sender
    message['To']=receiver

    smtpOBJ=SMTP_SSL('smtp.qq.com')
    smtpOBJ.login(user=mail_sender,password=mail_password)
    smtpOBJ.sendmail(mail_sender,receiver,str(message))
    smtpOBJ.quit()


def send_email_game(receiver, game_name, game_start_time, remaining_time):
    content=f"<br/>您预约/订阅/感兴趣的赛事<span style='color:red;font-size:20px;'>{game_name}</span>还有<span style='color:red;font-size:20px;'>{remaining_time}</span>开始，请及时查看，谢谢!"
    #实例化邮件对象，并制定关键参数
    message=MIMEText(content,'html','utf-8')
    #指定邮件的标题
    message['Subject']=Header('LetsCode-您预约/订阅/感兴趣的赛事即将开始，请及时查看，谢谢！','utf-8')
    message['From']=mail_sender
    message['To']=receiver

    smtpOBJ=SMTP_SSL('smtp.qq.com')
    smtpOBJ.login(user=mail_sender,password=mail_password)
    smtpOBJ.sendmail(mail_sender,receiver,str(message))
    smtpOBJ.quit()


def send_user_disable(receiver, user_name):
    content=f"<br/>您好，您的账户<span style='color:red;font-size:20px;'>{user_name}</span>已经被封禁，有问题可以联系我们的管理员，谢谢!"
    #实例化邮件对象，并制定关键参数
    message=MIMEText(content,'html','utf-8')
    #指定邮件的标题
    message['Subject']=Header('LetsCode-您的账户已经被封禁！','utf-8')
    message['From']=mail_sender
    message['To']=receiver

    smtpOBJ=SMTP_SSL('smtp.qq.com')
    smtpOBJ.login(user=mail_sender,password=mail_password)
    smtpOBJ.sendmail(mail_sender,receiver,str(message))
    smtpOBJ.quit()

def send_user_open(receiver, user_name):
    content=f"<br/>您好，您的账户<span style='color:red;font-size:20px;'>{user_name}</span>已经被解封，欢迎回来，谢谢!"
    #实例化邮件对象，并制定关键参数
    message=MIMEText(content,'html','utf-8')
    #指定邮件的标题
    message['Subject']=Header('LetsCode-您的账户已经被解封，欢迎回来，谢谢！','utf-8')
    message['From']=mail_sender
    message['To']=receiver

    smtpOBJ=SMTP_SSL('smtp.qq.com')
    smtpOBJ.login(user=mail_sender,password=mail_password)
    smtpOBJ.sendmail(mail_sender,receiver,str(message))
    smtpOBJ.quit()


def gen_email_code():
    str=random.sample(string.ascii_letters+string.digits,6)
    return ''.join(str)

def gen_register_code():
    return ''.join(random.sample(string.digits,6))


def gen_password():
    return ''.join(random.sample(string.ascii_letters+string.digits,8))


#单个模型类转化为标准json
def model_list(result):
    list = []
    for obj1, obj2 in result:
        dict = {}
        for k1, v1 in obj1.__dict__.items():
            if not k1.startswith('_sa_instance_state'):
                dict[k1] = v1

        list.append(dict)
    return list

#SQLAlchemy连接查询2张表转换为JSON[{},{},...]
def model_join_list(result):
    list=[]
    for obj1,obj2 in result:
        dict={}
        for k1,v1 in obj1.__dict__.items():
            if not k1.startswith('_sa_instance_state'):
                dict[k1]=v1
        for k2,v2 in obj1.__dict__.items():
            if not k2.startswith('_sa_instance_state'):
                if not k2 in dict:
                    dict[k2]=v2
        list.append(dict)
    return list


def get_duration(seconds):
    if seconds > 0 and seconds <= 3600:
        duration = '1'
    elif seconds > 3600 and seconds <= 3600 * 3:
        duration = '2'
    elif seconds > 3600 * 3 and seconds <= 3600 * 8:
        duration = '3'
    elif seconds > 3600 * 8 and seconds <= 3600 * 24:
        duration = '4'
    elif seconds > 3600 * 24:
        duration = '5'
    else:
        duration = '0'
    return duration

if __name__=='__main__':
    str='洛谷,LeetCode力扣,牛客网,AtCoder,CodeForces'
    print(len(str))


