from flask import Flask, render_template, request, jsonify, current_app
import os
import traceback
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS
import pymysql
from flask_cors import *



pymysql.install_as_MySQLdb()  # 解决版本兼容性问题

app = Flask(__name__)

app.config['SECRET_KEY'] = os.urandom(24)  # 产生sessionid
# 跨域请求
# CORS(app)

# 使用集成方式连接处理sqlalchemy，建议大家密码都改成 commonpass
# app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://root:root123@localhost:3306/letscode?charset=utf8"
app.config['SQLALCHEMY_DATABASE_URI'] = "mysql://stu:Stu_1234@120.92.144.134:53306/stu?charset=utf8"

app.config['SQLALCHEMY_POOL_SIZE']=2
app.config['SQLALCHEMY_POOL_RECYCLE']=50
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # 跟踪数据库的修改，及时发送信号
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # 跟踪数据库的修改，及时发送信号
# 实例化db对象
db = SQLAlchemy(app)

CORS(app,supports_credentials=True,resources={r"/*"})

@app.errorhandler(404)
def page_not_found(e):
    return render_template('error_404.html')

@app.errorhandler(500)
def server_error(e):
    return render_template('error_500.html')

# @app.route('/')
# def home():
#     return ('letscode')


@app.before_request
def before_request():
    if request.is_json:
        app.logger.info('recv request, %s, json:%s' % (request, request.json))
    else:
        app.logger.info('recv request, %s' % request)


@app.after_request
def after_request(response):
    if response.is_json:
        app.logger.info('resp response, %s, json:%s' % (response, response.json))
    else:
        app.logger.info('resp response, %s' % response)

    return response


def init_log(app):
    import logging
    from logging.handlers import RotatingFileHandler

    logging.basicConfig(level=logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s|%(pathname)040s:%(lineno)04d|%(levelname)4s|%(message)s', \
            '%Y-%m-%d %H:%M:%S')
    
    log_path = os.path.join(app.root_path, 'log')
    if not os.path.exists(log_path):
        os.makedirs(log_path)
    log_filename = os.path.join(log_path, 'stu.log')
    debug_handler = RotatingFileHandler(log_filename, maxBytes = 1024*1024, backupCount = 7, delay = False)
    debug_handler.setLevel(logging.DEBUG)
    debug_handler.setFormatter(formatter)
    app.logger.addHandler(debug_handler)
    
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    ch.setFormatter(formatter)
    app.logger.addHandler(ch)



if __name__ == '__main__':
    from controller.comment import *

    app.register_blueprint(comment)

    from controller.game import *
    app.register_blueprint(game)

    from controller.post import *
    app.register_blueprint(post)

    from controller.reserve import *
    app.register_blueprint(reserve)

    from controller.subscribe import *
    app.register_blueprint(subscribe)

    from controller.user import *
    app.register_blueprint(user)

    from controller.info import *
    app.register_blueprint(info)

    from controller.push import *
    app.register_blueprint(push)

    from controller.recommend import *
    app.register_blueprint(recommend)
    
    from controller.site_mail import *
    app.register_blueprint(site_mail)
    
    app.jinja_env.auto_reload = True

    init_log(app)
    
    if not app.debug or os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
        from common.schedule import scheduler
        scheduler.init_app(app)
        scheduler.start()

    app.run(host='0.0.0.0', port=8080, debug = True)