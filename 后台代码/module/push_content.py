from sqlalchemy import Table
from common.database import dbconnect

import time

dbsession,md,DBase=dbconnect()


class PushContent(DBase):
    __table__ = Table('push_content', md, autoload=True)
    #以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_id(cls, user_id):
        try:
            res = dbsession.query(PushContent).filter_by(user_id = user_id).all()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def query_by_key(cls, user_id, game_id, push_time, push_method):
        try:
            res = dbsession.query(PushContent).filter_by(user_id = user_id, game_id = game_id, push_time = push_time, push_method = push_method).all()
            return res
        finally:
            dbsession.close()
    
    def to_json(self):
        resp = {}
        for k,v in self.__dict__.items():
            if not k.startswith('_sa'):
                resp[k] = v
        return resp
