from sqlalchemy import Table
from common.database import dbconnect

from datetime import datetime

dbsession,md,DBase=dbconnect()


class SiteMail(DBase):
    __table__ = Table('site_mail', md, autoload=True)
    #以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_id(cls, user_id, page_num, page_size):
        try:
            res = dbsession.query(SiteMail).filter_by(user_id = user_id).order_by(SiteMail.mail_id.desc())\
                            .paginate(page_num, per_page = page_size)
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def get_by_id(cls, mail_id):
        try:
            res = dbsession.query(SiteMail).filter_by(mail_id=mail_id).first()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def delete_by_id(cls, mail_id):
        try:
            dbsession.query(SiteMail).filter_by(mail_id=mail_id).delete()
            dbsession.commit()
            return
        finally:
            dbsession.close()
    
    @classmethod
    def status_update(cls, mail_id, mail_status):
        try:
            dbsession.query(SiteMail).filter_by(mail_id=mail_id).update({'mail_status' : mail_status})
            dbsession.commit()
            return
        finally:
            dbsession.close()
    
    @classmethod
    def status_update_user_all(cls, user_id, mail_status):
        try:
            dbsession.query(SiteMail).filter_by(user_id=user_id).update({'mail_status' : mail_status})
            dbsession.commit()
            return
        finally:
            dbsession.close()
    
    def to_json(self):
        resp = {}
        for k,v in self.__dict__.items():
            if not k.startswith('_sa'):
                if type(v) == datetime:
	                resp[k]=v.strftime('%Y-%m-%d %H:%M:%S')
                else:
                    resp[k] = v
        return resp
