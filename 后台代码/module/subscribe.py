from sqlalchemy import Table, Column, Integer, String, Boolean
from common.database import dbconnect
import time

dbsession, md, DBase = dbconnect()


class Subscribe(DBase):
    __table__ = Table('Subscribe', md, autoload=True)

    # 以下为自定义的方法

    def update_subscribe(self):
        try:
            dbsession.add(self)
            dbsession.commit()
            return
        finally:
            dbsession.close()

    def delete_subscribe(self):
        try:
            dbsession.query(Subscribe).filter_by(subscribe_id=self.subscribe_id).delete()
            dbsession.commit()
            return
        finally:
            dbsession.close()

    @classmethod
    def delete_subscribe(cls, user_id):
        dbsession.query(Subscribe).filter_by(user_id=self.user_id).delete()
        dbsession.commit()
        #dbsession.close()
        return
        
    def get_by_user_id(self,u_id):
        try:
            subscribes = dbsession.query(Subscribe).filter_by(user_id=u_id).first()
            return subscribes
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_and_type(cls,u_id, g_type):
        try:
            subscribes = dbsession.query(Subscribe).filter_by(user_id=u_id, game_type=g_type).all()
            return subscribes
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_and_level(cls,u_id, lev):
        try:
            subscribes = dbsession.query(Subscribe).filter_by(user_id=u_id, level_=lev).all()
            return subscribes
        finally:
            dbsession.close()

    @classmethod
    def get_by_user_and_plat(cls,u_id, plat):
        try:
            subscribes = dbsession.query(Subscribe).filter_by(user_id=u_id, platform=plat).all()
            return subscribes
        finally:
            dbsession.close()
    
    @classmethod
    def modify(cls, user_id, game_platform, game_level, game_duration):
        data = {}

        if game_platform:
            data['game_platform'] = game_platform

        if game_level:
            data['game_level'] = game_level

        if game_duration:
            data['game_duration'] = game_duration

        try:
            dbsession.query(Subscribe).filter_by(user_id = user_id).update(data)
            dbsession.commit()
            return  {"info": "success" ,"code": 0}
        except:
            return {"info": "error" ,"code": 1}
        finally:
            dbsession.close()
    
    @classmethod
    def delete_by_user_id(cls, user_id):
        try:
            dbsession.query(Subscribe).filter_by(user_id=user_id).delete()
            dbsession.commit()
            return
        finally:
            dbsession.close()
    
    def to_json(self):
        resp = {}
        for k,v in self.__dict__.items():
            if not k.startswith('_sa'):
                resp[k] = v
        return resp
