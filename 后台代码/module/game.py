from sqlalchemy import Table
from common.database import dbconnect
from datetime import datetime
import time

dbsession, md, DBase = dbconnect()


class Game(DBase):
    __table__ = Table('game', md, autoload=True)

    # 以下为自定义的方法
    def add(self):
        try:
            dbsession.add(self)
            dbsession.commit()
        finally:
            dbsession.close()

    def rm(self):
        try:
            dbsession.delete(self)
            dbsession.commit()
        finally:
            dbsession.close()

    # 获取所有比赛
    def get_all(self):
        try:
            res = dbsession.query(Game).filter().all()
            return res
        finally:
            dbsession.close()

    #获取所有未审核
    def get_all_uchecked(self):
        try:
            res = dbsession.query(Game).filter(Game.checked==0).all()
            return res
        finally:
            dbsession.close()

    # 获取指定时间后的所有比赛，参数为datetime类型
    def get_after_date(self,mytime):
        try:
            res = dbsession.query(Game).filter(Game.game_start_time.__gt__(mytime)).all()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def by_start_time_range(cls, start_time, end_time):
        try:
            res = dbsession.query(Game).filter(Game.game_start_time.__gt__(start_time), Game.game_start_time.__lt__(end_time)).all()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def by_start_time_end(cls, end_time):
        try:
            res = dbsession.query(Game).filter(Game.game_start_time.__lt__(end_time)).all()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def get_by_id(cls,game_id):
        try:
            res = dbsession.query(Game).filter_by(game_id=game_id).first()
            return res
        finally:
            dbsession.close()

    def get_by_name(self,name):
        try:
            res = dbsession.query(Game).filter_by(game_name=name).first()
            return res
        finally:
            dbsession.close()

    def get_by_site(self,site):
        try:
            res = dbsession.query(Game).filter_by(website=site).first()
            return res
        finally:
            dbsession.close()

    def get_by_type(self,mtype):
        try:
            res = dbsession.query(Game).filter_by(game_type=mtype).all()
            #dbsession.close()
            return res
        finally:
            dbsession.close()

    def get_by_level(self,level):
        try:
            res = dbsession.query(Game).filter_by(level_=level).all()
            #dbsession.close()
            return res
        finally:
            dbsession.close()

    @classmethod
    def get_by_platform(cls, platform):
        try:
            res = dbsession.query(Game).filter_by(platform=platform).all()
            #dbsession.close()
            return res
        finally:
            dbsession.close()

    @classmethod
    def get_by_platform_level(cls, platform, level):
        try:
            res = dbsession.query(Game).filter_by(platform=platform, level_= level).all()
            #dbsession.close()
            return res
        finally:
            dbsession.close()
        
    def set_checked_by_id(self,game_id):
        try:
            res=dbsession.query(Game).filter_by(game_id=game_id).update({'checked':1})
            dbsession.commit()
            #dbsession.close()
        finally:
            dbsession.close()
    
    @classmethod
    def modify(cls, game_id, data):
        try:
            dbsession.query(Game).filter_by(game_id=game_id).update(data)
            dbsession.commit()
            #dbsession.close()
        finally:
            dbsession.close()
    
    @classmethod
    def get_by_cust_user_id(cls, cust_user_id):
        try:
            res = dbsession.query(Game).filter_by(cust_user_id = cust_user_id).all()
            #dbsession.close()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def get_by_cust(cls):
        try:
            res = dbsession.query(Game).filter(Game.cust_user_id != 0).all()
            #dbsession.close()
            return res
        finally:
            dbsession.close()
    
    @classmethod
    def delete_by_game_id(cls, game_id):
        try:
            dbsession.query(Game).filter_by(game_id = game_id).delete()
            dbsession.commit()
            #dbsession.close()
            return
        finally:
            dbsession.close()
    
    def to_json(self):
        resp = {}
        for k,v in self.__dict__.items():
	        if not k.startswith('_sa'):
	            if 'level_' == k:
	                resp['level']=v
	            elif type(v) == datetime:
	                resp[k]=v.strftime('%Y-%m-%d %H:%M:%S')
	            else:
	                resp[k]=v
        return resp

#
# # 获取所有比赛
# def get_all_game():
#     res = dbsession.query(Game).all()
#     return res
#
#
# # 获取指定时间后的所有比赛，参数为datetime类型
# def get_game_after_date(mytime):
#     res = dbsession.query(Game).filter(Game.game_start_time.__gt__(mytime)).all()
#     return res
#
#
# def get_game_by_name(name):
#     res = dbsession.query(Game).filter_by(name=name).first()
#     return res
#
#
# def get_game_by_site(site):
#     res = dbsession.query(Game).filter_by(website=site).first()
#     return res
#
#
# def get_game_by_type(mtype):
#     res = dbsession.query(Game).filter_by(game_type=mtype).all()
#     return res
#
#
# def get_game_by_level(level):
#     res = dbsession.query(Game).filter_by(level_=level).all()
#     return res
#
#
# def get_game_by_platform(platform):
#     res = dbsession.query(Game).filter_by(platform=platform).all()
#     return res